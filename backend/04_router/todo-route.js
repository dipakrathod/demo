const express = require("express")
const { postuser, getuser, deleteuser, updateuser } = require("../03_controller/todo-controller")

const router = express.Router()

router.route("/post").post(postuser)
router.route("/get").get(getuser)
router.route("/delete/:id").delete(deleteuser)
router.route("/update/:id").put(updateuser)
module.exports = router



