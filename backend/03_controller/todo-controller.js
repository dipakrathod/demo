const user = require("../02_model/Todo-model")

exports.postuser =async(req,res)=>{
    try {
    const result = await user.create(req.body)
    res.json({
        success:true,
        message:"user created",
        data:result
    })
   } catch (error) {
    res.json({
        success:true,
        message:"something went wrong in controller"+ error,
        data:null
    })
   }

}

exports.getuser =async(req,res)=>{
    try {
    const result = await user.find()
    res.json({
        count:result.length,
        success:true,
        message:"get user ",
        data:result
    })
   } catch (error) {
    res.json({
        success:true,
        message:"something went wrong in controller"+ error,
        data:null
    })
   }

}
exports.deleteuser =async(req,res)=>{
    try {
    const result = await user.findByIdAndDelete(req.params.id)
    res.json({
        
        success:true,
        message:"user deleted",
        data:result
    })
   } catch (error) {
    res.json({
        success:true,
        message:"something went wrong in controller"+ error,
        data:null
    })
   }

}
exports.updateuser =async(req,res)=>{
    try {
    const result = await user.findByIdAndUpdate(req.params.id, req.body ,{new:true})
    res.json({
        
        success:true,
        message:"user updated",
        data:result
    })
   } catch (error) {
    res.json({
        success:true,
        message:"something went wrong in controller"+ error,
        data:null
    })
   }

}

