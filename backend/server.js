const express = require('express')
const app = express()
const cors = require("cors")
const env = require("dotenv")
env.config({path:"./01_config/.env"})

const db  = require("./01_config/db")
db()


app.use(express.json())
app.use(cors())

const user = require("./04_router/todo-route")
app.use("/api/v1", user)

app.listen(process.env.PORT || 5000, () => console.log(`http://localhost:${process.env.PORT}`))