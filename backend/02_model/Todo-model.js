const mongoose = require("mongoose")

const TodoSchema = mongoose.Schema({
    username:{
              type:String,
              required:true,
          }
},{timestamps:true})

module.exports = mongoose.model("redux-todo" , TodoSchema )